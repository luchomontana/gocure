# Changelog
All notable changes to this project will be documented in this file.

## v22.04.18
### Added
- An option to generate a HTML report from Embeded page.
- Changelog page to documentation.
- Add dowload all button for embed files and html reports

### Changed
- Change the documentation url at website footer.
- Fix issue to show default values.
- Fix close button embedded files layout.
- Change sidebar gocure link.
- Fix embed model title layout to support big titles.

### Removed
- Remove print icon from audio and video files
  
### [Download](https://gitlab.com/rodrigoodhin/gocure/-/releases/v22.04.18)

## v22.03.01 
First oficial release

### Added
- dockerfile with a all functionalities.
- website to generate html reports and embed files.
- rest api to embed and generate html files.
- embed files functionality.

### Changed
- new html report layout.
- fix some bugs.

### Removed

### [Download](https://gitlab.com/rodrigoodhin/gocure/-/releases/v22.03.01)

